// Import the functions you need from the SDKs you need
import { initializeApp, getApp, getApps } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyAwaAukMZ3r80Vg2OKWuFGbIO6O-m5Zqmg",
    authDomain: "netflix-clone-88b31.firebaseapp.com",
    projectId: "netflix-clone-88b31",
    storageBucket: "netflix-clone-88b31.appspot.com",
    messagingSenderId: "955482683240",
    appId: "1:955482683240:web:357ad2bea06ba3063788dd"
};

// Initialize Firebase
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
// const db = getFirestore();
const auth = getAuth();

export default app;
export { auth };
